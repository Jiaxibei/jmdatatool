# JMDataTool

[![CI Status](https://img.shields.io/travis/1102287306@qq.com/JMDataTool.svg?style=flat)](https://travis-ci.org/1102287306@qq.com/JMDataTool)
[![Version](https://img.shields.io/cocoapods/v/JMDataTool.svg?style=flat)](https://cocoapods.org/pods/JMDataTool)
[![License](https://img.shields.io/cocoapods/l/JMDataTool.svg?style=flat)](https://cocoapods.org/pods/JMDataTool)
[![Platform](https://img.shields.io/cocoapods/p/JMDataTool.svg?style=flat)](https://cocoapods.org/pods/JMDataTool)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JMDataTool is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JMDataTool'
```

## Author

1102287306@qq.com, 17737812427@163.com

## License

JMDataTool is available under the MIT license. See the LICENSE file for more info.
